$(document).ready(function(){
    
    var $nb_parts;
    var $nb_pizza;
 	var $total=0;
 	var $number=0;
 	var $inter=0.0;
 	var $parts=0.0;
 	var $sstotal=0;

 	//Afficher ingrédients en survolant la pizza
    $("label").hover(function(){
        $(this).find("span").toggle(0,"display");
    })

    //Afficher visuellement le nombre de part de pizza
	$( ".nb-parts input" ).on("input",function() {
		//On enleve ce qu'il y a d'afficher
		$(".pizza-pict").remove();
		$(this).after("<span class='pizza-1 pizza-pict'></span>");
		$(".pizza-pict").removeClass().addClass("pizza-pict pizza-0");
		//On recupere la nouvelle valeur du nombre de parts
		$nb_parts = $(this).val();
		//On divise par 6 pour avoir le nombre de pizza
		$nb_pizza = $nb_parts/6;
		//Boucle for qui permet d'afficher plusieur pizzas
		if($nb_pizza>1)
		{
			for(var i=0;i<$nb_pizza-1;i++)
			{
				$(".pizza-pict").last().addClass('pizza-6');
				//Condition permettant de blinder l'affichage du nombre de parts
				if($nb_parts%6>=1)
				{
					$(".pizza-pict").last().after("<span class='pizza-"+$nb_parts%6+" pizza-pict'></span>");
				}
				else
				{
					$(".pizza-pict").last().after("<span class='pizza-"+6+" pizza-pict'></span>");
				}
			}
		}
		else
		{
			$(".pizza-pict").addClass("pizza-"+$nb_parts);
		}
  	})

	//Lorsqu'on clique sur le bouton suivant -> on l'enleve -> on affiche le formulaire infos client
  	$(".next-step").click(function(){
  		$(this).remove();
  		$(".infos-client").show();
  	})

  	//Ajouter une ligne au champ address
  	$(".add").click(function(){
  		$(this).before("<br><input type='text'/>");
  	})

  	//Lorsqu'on clique sur le bouton valider -> on remplace tous les éléments de la page par message de remerciement
  	$(".done").click(function(){
  		var $name = $(".infos-client input").val();
  		$("<h2>Merci "+$name+" ! Votre commande sera livrée dans 15min.</h2>").replaceAll(".main");
  	})

  	//Mettre a jour le prix en fonction des options cochées
  	//On detecte quand l'un des checkbox et radio button change d'état
  	$("input[type=checkbox], input[type=radio]").change(function(){
  		$total=0;
  		$inter=0.0;
  		$parts=0.0;
  		$sstotal=0;
  		//On parcours chaque checkbox et radio button coché
  		$("input[type=checkbox]:checked, input[type=radio]:checked").each(function(){ 

  			//Pour chaque checkbox ou radio button coché on recupere la valeur de l'attribut data-price
  			$number= $(this).attr("data-price");

  			//Si le prix correspond a une pizza -> calcul en fonction du nombre de parts
  			if($number=="8"||$number=="10"||$number=="11"||$number=="12")
  			{
  				//Permet d'avoir le prix d'une part de pizza
  				$inter=$number/6;
  				//On recupere le nombre de parts
  				$parts= $(".nb-parts input").val();
  				//On multiplie le prix unitaire de la part en fonction du nombre de parts
  				$sstotal=$parts*$inter;
  				//On met à jour le prix total
  				$total= $total+$sstotal;
  			}
  			else
  			{
  				//On met la valeur recupere en int et on fait le calcul
  				//Les extras et pates ne sont comptés qu'une seule fois peu importe le nombre de part
  				$total= $total+parseInt($number); 
  			}			
  		})
  		//On affiche le nouveau prix
  		$("<p>"+$total+" €</p>").replaceAll(".tile > p");
  	})
})